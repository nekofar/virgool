# Copyright (C) 2019 Milad Nekofar
# This file is distributed under the same license as the Virgool plugin.
msgid ""
msgstr ""
"Project-Id-Version: Virgool 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/virgool\n"
"POT-Creation-Date: 2019-07-10T07:59:36+00:00\n"
"PO-Revision-Date: 2019-07-04 15:05+0430\n"
"Last-Translator: Milad Nekofar <milad@nekofar.com>\n"
"Language-Team: \n"
"Language: fa_IR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"X-Domain: virgool\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Plugin Name of the plugin
#: admin/class-virgool-admin.php:92
msgid "Virgool"
msgstr "ویرگول"

#. Plugin URI of the plugin
msgid "https://github.com/nekofar/virgool"
msgstr "https://github.com/nekofar/virgool"

#. Description of the plugin
msgid "Virgool lets you publish posts automatically to a Virgool profile."
msgstr ""
"ویرگول امکان انتشار خودکار مطالب را در پرو فایل ویرگول شما فراهم می کند."

#. Author of the plugin
msgid "Milad Nekofar"
msgstr "میلاد نکوفر"

#. Author URI of the plugin
msgid "https://milad.nekofar.com/"
msgstr "https://milad.nekofar.com/"

#: admin/class-virgool-admin.php:73
msgid "Settings"
msgstr "تنظیمات"

#: admin/class-virgool-admin.php:74
msgid "Support"
msgstr "پشتیبانی"

#: admin/class-virgool-admin.php:91
msgid "Virgool Settings"
msgstr "تنظیمات ویرگول"

#: admin/class-virgool-admin.php:123
msgid "Login Information"
msgstr "اطلاعات ورود"

#: admin/class-virgool-admin.php:130
msgid "Publish Settings"
msgstr "تنظیمات انتشار"

#: admin/class-virgool-admin.php:137
msgid "Username"
msgstr "نام کاربری"

#: admin/class-virgool-admin.php:143
msgid "The username you use to enter the Virgool."
msgstr "نام کاربری که برای ورود به ویرگول استفاده می کنید."

#: admin/class-virgool-admin.php:149
msgid "Password"
msgstr "رمز عبور"

#: admin/class-virgool-admin.php:155
msgid "The password you use to enter the Virgool."
msgstr "رمز عبوری که برای ورود به ویرگول استفاده می کنید."

#: admin/class-virgool-admin.php:161
msgid "Status"
msgstr "وضعیت"

#: admin/class-virgool-admin.php:167
msgid "Default status of the publication of the contents sent to the virgool."
msgstr "وضعیت پیش فرض انتشار مطالب ارسال شده به ویرگول."

#: admin/class-virgool-admin.php:178
msgid ""
"This plugin uses your login information to communicate with the Virgool. "
"Please enter your login information as requested."
msgstr ""
"این افزونه برای ارتباط با ویرگول از اطلاعات ورود شما استفاده می کند. لطفا "
"اطلاعات ورود خواسته شده را وارد کنید."

#: admin/class-virgool-admin.php:187
msgid ""
"Using this section, you can choose the options for publishing the content in "
"the Virgool."
msgstr ""
"با استفاده از این بخش، می توانید گزینه هایی را برای انتشار محتوا در ویرگول "
"انتخاب کنید."

#: admin/class-virgool-admin.php:260 admin/class-virgool-admin.php:300
msgid "Draft"
msgstr "پیش نویس"

#: admin/class-virgool-admin.php:261 admin/class-virgool-admin.php:301
msgid "Published"
msgstr "منتشر شده"

#: admin/class-virgool-admin.php:326
msgid "Cross post to the Virgool"
msgstr "ارسال متقابل به ویرگول"

#. translators: %s: number of success.
#: admin/class-virgool-admin.php:401
msgid "%s post crossed to the virgool successfully."
msgid_plural "%s posts crossed to the virgool successfully"
msgstr[0] "%s نوشته با موفقیت به ویرگول ارسال شد."

#. translators: %s: number of failure.
#: admin/class-virgool-admin.php:407
msgid "Failed to cross %s post crossed to the virgool."
msgid_plural "Failed to cross %s posts crossed to the virgool."
msgstr[0] "ارسال %s نوشته به ویرگول شکست خورد."

#: includes/class-virgool-api.php:68
msgid "Login to the virgool api has been failed."
msgstr "ورود به ویرگول ناموفق بود."

#: includes/class-virgool-api.php:97
msgid "Retrieve user info has been failed."
msgstr "دریافت اطلاعات کاربر ناموفق بود."

#: includes/class-virgool-api.php:113 includes/class-virgool-api.php:146
msgid "Wrong post status has been selected."
msgstr "وضعیت پست انتخاب شده صحیح نیست."

#: includes/class-virgool-api.php:129
msgid "Retrieve user posts has been failed."
msgstr "دریافت ارسال های کاربر ناموفق بود."

#: includes/class-virgool-api.php:172
msgid "Create user post has been failed."
msgstr "ایجاد پست جدید برای کاربر ناموفق بود."

#: includes/class-virgool-api.php:229 includes/class-virgool-api.php:234
msgid "Upload primary image has been failed."
msgstr "بارگذاری تصویر اصلی شکست خورد."

#~ msgid "Default status of the publication of the contents sent to the comma."
#~ msgstr "وضعیت پیش فرض انتشار مطالب ارسال شده به ویرگول."

#~ msgid "Crossed %s post to Virgool."
#~ msgid_plural "Crossed %s posts to Virgool."
#~ msgstr[0] "%s نوشته متقابلا به ویرگول ارسال شد."
